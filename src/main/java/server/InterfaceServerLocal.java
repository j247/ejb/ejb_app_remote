package server;

import javax.ejb.Local;

@Local
public interface InterfaceServerLocal 
{
	public String consult(int id);
}