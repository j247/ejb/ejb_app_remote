package server;

public class Employee 
{
	private int id;
	private String name;
	private String email;
	private String position;
	private double salary;
	
	public Employee(int id, String name, String email, String position, double salary) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.position = position;
		this.salary = salary;
	}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getEmail() {
		return email;
	}
	public String getPosition() {
		return position;
	}
	public double getSalary() {
		return salary;
	}	
}
