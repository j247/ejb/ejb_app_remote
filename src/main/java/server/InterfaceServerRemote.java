package server;

import javax.ejb.Remote;

@Remote
public interface InterfaceServerRemote 
{
	public String consult(int id);
}
