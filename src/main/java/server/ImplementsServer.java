package server;

import java.util.ArrayList;
import javax.ejb.Stateless;

@Stateless(name="ServiceConsult")
public class ImplementsServer implements InterfaceServerLocal, InterfaceServerRemote
{
	private static ArrayList<Employee> listEmployees()
	{
		ArrayList<Employee> list = new ArrayList<Employee>();
		
		list.add(new Employee(1,"Laura Leen", "lau@ejb.com","Diseņadora", 2300000));
		list.add(new Employee(2,"Mey Chan", "mey@ejb.com","Diseņadora", 2300000));
		list.add(new Employee(3,"Charles Dau", "charles@ejb.com","Editor", 2000000));
		list.add(new Employee(4,"Peter Lin", "peter@ejb.com","Editor", 2000000));
	
		return list;
	}
	
	private static String getEmployee(int id) 
	{
				
		 return "Nombre: " + listEmployees().get(id-1).getName()+"<br>\n"
				+"Email: " + listEmployees().get(id-1).getEmail()+"<br>\n"
				+"Cargo: " + listEmployees().get(id-1).getPosition()+"<br>\n"
				+"Salario: " + listEmployees().get(id-1).getSalary();
	}
	
	public String consult(int id) {
		
		if(id < listEmployees().size()+1)
		{
			return getEmployee(id);			
		}
		else {
			return "No existe registro con el ID ingresado";
		}
	}
}
