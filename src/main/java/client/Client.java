package client;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import server.InterfaceServerLocal;

@WebServlet(urlPatterns="/cons")
public class Client extends HttpServlet 
{
	
	private static final long serialVersionUID = 1L;
	@EJB private InterfaceServerLocal service;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException 
	{
		int id=Integer.parseInt(req.getParameter("id"));
		PrintWriter pw= resp.getWriter();
		
		String result=service.consult(id);
		
		pw.println("<h1>Datos de Empleado</h1>");
		pw.println("<p>"+result+"</p>");
				
		pw.flush();
		pw.close();
		
	}
}
